using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeUI : MonoBehaviour
{
    public GameObject _ui;
    private Node _target;
    public TMP_Text sellAmount;
    
    public void SetTarget(Node target)
    {
        _target = target;
        transform.position = _target.GetBuildPosition();
        sellAmount.text = "$" + target._turretBlueprint.GetSellAmount();
        _ui.SetActive(true);
    }

    public void Hide()
    {
        _ui.SetActive(false);
    }
    public void Sell()
    {
        _target.SellTurret();
        BuildManager.instanse.DeselectNode();
    }
}   
