using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ProjectTypeMovement : MonoBehaviour
{
    public Transform _b;
    public Transform _a;
    public Transform _control;
    public GameObject _bullet;
    private Enemy _targetEnemy;
    public float _maxRange, _minRange, _height, _fireCountDown, _countDown;

    private void Start() 
    {
        _a = transform;
        InvokeRepeating("UpdateTarget",0, 2f);
    }
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float _shortestDistance = Mathf.Infinity;
        GameObject _nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float _distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (_distanceToEnemy < _shortestDistance && _distanceToEnemy >= _minRange)
            {
                _shortestDistance = _distanceToEnemy;
                _nearestEnemy = enemy;
            }
        }

        if(_nearestEnemy != null && _shortestDistance <= _maxRange && _shortestDistance > _minRange)
        {
            _b = _nearestEnemy.transform;
            
            controlPoint();
            _targetEnemy = _nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            _b = null;
        }
    }
    private void controlPoint()
    {
        if (_b != null)
        {
            _control.position =  (_b.position + _a.position) / 2 + new Vector3(0, _height, 0);
        }
        
        
    }
    public Vector3 evaluate(float t)
    {
        
        Vector3 ac = Vector3.Lerp(_a.position, _control.position, t);
        Vector3 cb = Vector3.Lerp(_control.position, _b.position, t);
        return Vector3.Lerp(ac, cb, t);
    }

    private void Update() 
    {
        _fireCountDown -= Time.deltaTime;
        if(_fireCountDown <= 0 && _b != null && _control != null)
        {
            GameObject bullet =  Instantiate(_bullet, transform.position, transform.rotation);
            ProjectTile bulletPath = bullet.GetComponent<ProjectTile>();
            bulletPath.Init(_a.position, _control.position, _b.position);
            bulletPath._curve = this;
            _fireCountDown = _countDown;
        }
    }

    private void OnDrawGizmos() {
        if(_b == null || _a == null || _control == null)
        {
            return;
        }

        for (int i = 0; i < 20; i++)
        {
            Gizmos.DrawWireSphere(evaluate(i / 20f), 0.1f);
        }
    }
}
