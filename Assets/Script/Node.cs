using UnityEngine;
using UnityEngine.EventSystems;


public class Node : MonoBehaviour
{
    public Color _hoverColor;
    public Color _notEnoughMoney;
    public Vector3 _positionOffSet;
    public TurretBluePrint _turretBlueprint;
    public GameObject _turret;
    private Renderer _rend;
    private Color _startColor;
    BuildManager buildManager;

    void Start()
    {
        _rend = GetComponent<Renderer>();
        _startColor = _rend.material.color;

        buildManager = BuildManager.instanse;
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + _positionOffSet;
    }

    void OnMouseDown() 
    {
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if(_turret != null)
        {
            buildManager.SelecNode(this);
            return;
        }
        if(!buildManager.CanBuild)
        {
            return;
        }
        buildManager.BuildTurretOn(this);
    }
    public void SellTurret()
    {
        PlayerStats._money += _turretBlueprint.GetSellAmount();
        Destroy(_turret);
        _turretBlueprint = null;
    }
    void OnMouseEnter()
    {
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if(!buildManager.CanBuild)
        {
            return;
        }
        if(buildManager.HasMoney)
        {
            _rend.material.color = _hoverColor;
        }
        else
        {
            _rend.material.color = _notEnoughMoney;
        }
    }

    void OnMouseExit()
    {
        _rend.material.color = _startColor;
    }
}
