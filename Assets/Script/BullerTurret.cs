using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullerTurret : MonoBehaviour
{
    public enum Effect
    {
        Slow, Burn
    }
    
    private Transform target;
    public float _speed = 70f;
    public int _damage = 50;
    public float _explosionRadius = 0f;
    [SerializeField] GameObject _impactEffect;
    [System.Serializable] public struct MakeEffects
    {
        [SerializeField] public Effect _effect;
        [SerializeField] public int _value;
        [SerializeField] public float _duration;
    }
    [SerializeField] MakeEffects[] _effects;
    // Start is called before the first frame update
    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distancethisFrame = _speed * Time.deltaTime;

        if (dir.magnitude <= distancethisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distancethisFrame, Space.World);
        transform.LookAt(target);
    }

    void HitTarget()
    {
        GameObject _effectIn = (GameObject)Instantiate(_impactEffect, transform.position, transform.rotation);
        Destroy(_effectIn, 5f);

        if(_explosionRadius > 0f)
        {
            Explode();
        } else 
        {
            Damage(target);
        }
        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _explosionRadius);
        foreach(Collider collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();
        if(e != null)
        {
            e.TakeDamage(_damage);
            TakeEffect();
        }
    }

    void TakeEffect()
    {
        foreach(MakeEffects effect in _effects)
        {
            StatusManager statusManager = target.GetComponent<StatusManager>();
            if(effect._effect == Effect.Burn)
            {
                statusManager._burnDuration = effect._duration;
                statusManager._burnDamage = effect._value;
            }  
            if(effect._effect == Effect.Slow)
            {
                statusManager._slowDuration = effect._duration;
                statusManager._slowValue = effect._value;
            }
        }    
    }

    void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _explosionRadius);
    }
}


