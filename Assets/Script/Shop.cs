using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBluePrint _standardTurret;
    public TurretBluePrint _missleLauncher;
    public TurretBluePrint _lazerBeam;
    public TurretBluePrint _flameThrower;
    BuildManager _buildManager;
    void Start() 
    {
        _buildManager = BuildManager.instanse;
    }
    public void SelectStandardTurret()
    {
        _buildManager.SeclectTurretToBuild(_standardTurret);
    }
    public void SeclectMissileLauncher()
    {
        _buildManager.SeclectTurretToBuild( _missleLauncher);
    }
    public void SeclectLazerBeam()
    {
        _buildManager.SeclectTurretToBuild( _lazerBeam);
    }
    public void SeclectFlameThrower()
    {
        _buildManager.SeclectTurretToBuild( _flameThrower);
    }

}
