using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Turret : MonoBehaviour
{
    private Enemy _targetEnemy;
    public Transform _Target;
    public Transform _partToRotate;
    public Transform _firePoint;
    public float _range = 15f;
    public float _turnSpeed = 10f;
    public float _fireRate;
    public GameObject _bulletPrefab;
    [SerializeField] private float _fireCountDown;
    
    [Header("Lazer")]
    public bool _useLazer = false;
    public int _damageOverTime = 30;
    public LineRenderer _lineRenderer;
    public ParticleSystem _lazerImpactEffect;
    public Light _impactLight;
    
    public string _enemyTag = "Enemy";
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(_enemyTag);
        float _shortestDistance = Mathf.Infinity;
        GameObject _nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float _distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (_distanceToEnemy < _shortestDistance)
            {
                _shortestDistance = _distanceToEnemy;
                _nearestEnemy = enemy;
            }
        }

        if(_nearestEnemy != null && _shortestDistance <= _range)
        {
            _Target = _nearestEnemy.transform;
            _targetEnemy = _nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            _Target = null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(_Target == null)
        {
            if(_useLazer)
            {
                if(_lineRenderer.enabled)
                {
                    _lineRenderer.enabled = false;
                    _lazerImpactEffect.Stop();
                    _impactLight.enabled = false;
                }
            }
            return;
        }

        //target lock on
        LockOnTarget();
        if(_useLazer)
        {
            Lazer();
        }
        else 
        {
            if(_fireCountDown <= 0)
            {
                Shoot();
                _fireCountDown = 1f/_fireRate;
            }
            _fireCountDown -= Time.deltaTime;
        }
        
    }

    void LockOnTarget()
    {
        Vector3 _dir = _Target.position - transform.position;
        Quaternion _lockRotate = Quaternion.LookRotation(_dir);
        Vector3 _rotation = Quaternion.Lerp(_partToRotate.rotation, _lockRotate, Time.deltaTime * _turnSpeed).eulerAngles;
        _partToRotate.rotation = Quaternion.Euler(0f, _rotation.y, 0f);

    }

    void Lazer()
    {
       _targetEnemy.TakeDamage(_damageOverTime * Time.deltaTime);

        if(!_lineRenderer.enabled)
        {
            _lineRenderer.enabled = true;
            _lazerImpactEffect.Play();
            _impactLight.enabled = true;
        }
        _lineRenderer.SetPosition(0, _firePoint.position);
        _lineRenderer.SetPosition(1, _Target.position);

        Vector3 dir = _firePoint.position - _Target.position;
        _lazerImpactEffect.transform.position = _Target.position + dir.normalized; 
        _lazerImpactEffect.transform.rotation = Quaternion.LookRotation(dir);
        
    }

    void Shoot()
    {
        GameObject _bulletGo = (GameObject)Instantiate(_bulletPrefab, _firePoint.position, _firePoint.rotation);
        BullerTurret _bullet = _bulletGo.GetComponent<BullerTurret>();

        if (_bullet != null)
        {
            _bullet.Seek(_Target);
        }
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _range); 
    }
}
