using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instanse;
    public NodeUI _nodeUI;
    private Node _selectNode;
    private TurretBluePrint _turretToBuild;


    void Awake() 
    {
        if(instanse != null)
        {
            return;
        }
        instanse = this;
    }

    public bool CanBuild
    {
        get{ return _turretToBuild != null;}
    }
    public bool HasMoney
    {
        get{ return PlayerStats._money >= _turretToBuild.cost;}
    }
    
    public void BuildTurretOn(Node node)
    {
        if(PlayerStats._money < _turretToBuild.cost)
        {
            return;
        }

        PlayerStats._money -= _turretToBuild.cost;

        GameObject turret = (GameObject)Instantiate(_turretToBuild.prefab, node.GetBuildPosition(), Quaternion.identity);
        node._turret = turret;
    }

    public void SelecNode(Node node)
    {
        if(_selectNode == node)
        {
            DeselectNode();
            return;
        }
        _selectNode = node;
        _turretToBuild = null;

        _nodeUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        _selectNode = null;
        _nodeUI.Hide();
    }

    public void SeclectTurretToBuild(TurretBluePrint turret)
    {
        _turretToBuild = turret;
        DeselectNode();
    }
}