using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectTile : MonoBehaviour
{
    public ProjectTypeMovement _curve;
    public float _speed;
    public GameObject _effectPrefabs;
private Vector3 posMoveTo;
    private float _sampleTime;
    Vector3 posStart,  posCenter, posEnd;
    // Start is called before the first frame update
    void Start()
    {
        _sampleTime = 0f;
    }
    public void Init(Vector3 posStart, Vector3 posCenter, Vector3 posEnd)
    {
        this.posStart = posStart;
        this.posCenter = posCenter;
        this.posEnd = posEnd;
    }

    // Update is called once per frame
    void Update()
    {
        if(_curve._b != null)
        {
            _sampleTime += Time.deltaTime * _speed;
            transform.position = Calculation(_sampleTime);
            transform.forward = Calculation(_sampleTime + 0.001f) - transform.position;

            if(_sampleTime >= 1f)
            {
                Instantiate(_effectPrefabs, transform.position, transform.rotation);
                Debug.Log("ooke");
                Destroy(gameObject);
            }
        } 
        else
        {
            Destroy(gameObject);
        }
    }  
    Vector3 Calculation(float height)
    {
        Vector3 ac = Vector3.Lerp(posStart, posCenter, height);
        Vector3 cb = Vector3.Lerp(posCenter, posEnd, height);
        return Vector3.Lerp(ac, cb, height);
    }
}
