using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float _speed;
    public float _trueSpeed;
    public float _startHealth = 100;
	private float _health;
    public int _worth = 50;
    private Transform _target;
    private int _wavepointIndex = 0;
    public Image _healthBar;
    // Start is called before the first frame update
    void Start()
    {
        _target = Waypoints._points[0];
    }

    public void TakeDamage(float amount)
    {
        _health -= amount;
        // _healthBar.fillAmount = _health;
        if(_health <= 0)
        {
            Debug.Log("Die");
            Die();
        }
    }

    void Die()
    {
        PlayerStats._money += _worth;
        Destroy(gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 _dir = _target.position - transform.position;
        transform.Translate(_dir.normalized * _trueSpeed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position, _target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }

    }

    void GetNextWaypoint()
    {
        if (_wavepointIndex >= Waypoints._points.Length - 1)
        {
            EndPath();
            return;
        }

        _wavepointIndex++;
        _target = Waypoints._points[_wavepointIndex];
    }

    void EndPath()
    {
        PlayerStats._lives--;
        Destroy(gameObject);
    }

    
}
