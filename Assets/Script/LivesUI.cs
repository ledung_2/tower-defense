using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour
{
    public TMP_Text _livesText;

    // Update is called once per frame
    void Update()
    {
        _livesText.text = PlayerStats._lives.ToString();
    }
}
