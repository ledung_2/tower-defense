using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WaveSpawn : MonoBehaviour
{
    public Transform _enemyPrefabs;
    public Transform _spawnPoint;
    public float _timeBattleWave = 5f;
    public TMP_Text _countDownText;

    private float _countDown = 4f;
    private int _waveIndex = 0;

    // Update is called once per frame
    void Update()
    {
        if(_countDown <= 0f)
        {   
            StartCoroutine(SpawnWave());
            _countDown = _timeBattleWave;
        }

        _countDown -= Time.deltaTime;
        _countDown = Mathf.Clamp(_countDown, 0f, Mathf.Infinity);
        _countDownText.text = string.Format("{0:00.00}", _countDown);
    }

    IEnumerator SpawnWave()
    {   
        _waveIndex++;
        PlayerStats._rounds++;

        for (int i = 0; i <= _waveIndex; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.5f);
        }    
    }

    void SpawnEnemy()
    {
        Instantiate(_enemyPrefabs, _spawnPoint.position, _spawnPoint.rotation);
    }
}
