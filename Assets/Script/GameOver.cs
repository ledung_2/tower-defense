using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public TMP_Text _rounsText;
    
    void OnEnable()
    {
        _rounsText.text = PlayerStats._rounds.ToString();
    }
    
    public void Retry()
    {
        
    }

    public void Menu()
    {

    }
}
