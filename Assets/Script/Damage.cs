using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float _damage;
    private void Start()
    {
        Destroy(gameObject, 3f);
    }
    
    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("hit" + other.gameObject);
        if(other.gameObject.CompareTag("Enemy"))
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            
            enemy.TakeDamage(_damage);
            Debug.Log("dame");
        } 
    }
}
