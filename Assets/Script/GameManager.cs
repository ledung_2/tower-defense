using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool _gameIsOver;
    public GameObject _gameOverUI;
    void Start() 
    {
        _gameIsOver = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(_gameIsOver)
        {
            return;
        }
        
        if(PlayerStats._lives <= 0)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        _gameIsOver = true;
        _gameOverUI.SetActive(true);
    }
}
