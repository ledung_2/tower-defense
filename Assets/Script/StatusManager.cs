using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using UnityEngine;

public class StatusManager : MonoBehaviour
{
    
    public float _burnDamage, _burnDuration;
    public float _slowValue, _slowDuration;
    private bool _check = true;
    private Enemy _enemyStatus;
    // Start is called before the first frame update
    void Start()
    {
        _enemyStatus = gameObject.GetComponent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        
        _burnDuration -= Time.deltaTime;
        _slowDuration -= Time.deltaTime;
        _burnDuration = Mathf.Clamp(_burnDuration, 0f, Mathf.Infinity);
        _slowDuration = Mathf.Clamp(_slowDuration, 0f, Mathf.Infinity);
        // burn damage
        if(_burnDuration > 0 && _check)
        {
            _check = false;
            StartCoroutine(Burn());
        }
        if(_slowDuration > 0)
        {
            _enemyStatus._trueSpeed = _enemyStatus._speed * (100 - _slowValue) * 0.01f;
            
        } else if(_slowDuration == 0)
        {
            _enemyStatus._trueSpeed = _enemyStatus._speed;
        }
    }

    IEnumerator Burn()
    { 
        _enemyStatus.TakeDamage(_burnDamage);
        yield return new WaitForSeconds(1f);
        _check = true;
    }
}
