using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static int _money;
    public int _startMoney = 400;

    public static int _lives;
    public int _startLive = 20;

    public static int _rounds;
    void Start()
    {
        _money = _startMoney;
        _lives = _startLive;

        _rounds = 0;
    }

    
}
