using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour
{
    public TMP_Text _moneyText;

    // Update is called once per frame
    void Update()
    {
        _moneyText.text = "$" + PlayerStats._money.ToString();
    }
}
